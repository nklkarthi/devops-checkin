# App changes

The cmd pinger is added with 1 more parameter PING_COUNT with default value as 5. This variable can be passed to the application as ENV variable. If passed, viper will use this to limit the number of pings

# Pipeline Design

## Stages

- build_test_local - stage to build and test the image locally

- build_image - stage to build the docker image containing the binary using dind

- test_image - stage to verify the docker image standalone by running using dind 

- test_compose - stage to verify the 2 instances of the container running using docker-compose

- verify_pipeline - stage to verify if the pipeline, README.md and artifact exists.

## Jobs

- build_test_local > build_app - job to build and test the image locally and produce binary artifact (available to download from the job)

- build_image > docker_image - job that uses the makefile  docker_image target to build image and produce image artifact (available to download from the job)

- test_image > docker_testimage - job that tests the existence of the image created using Google's container-structure-test utility 

- test_image > docker_testrun - job that runs the docker image created earlier and verify if the pinging working.

- test_compose > compose_testenv - job that runs the 2 container instances using docker compose and verify if the pinging between each other. 

- verify_pipeline > done? - to verify if the pipeline, README.md and artifact exists.

*** makefile docker_testrun target has -it options. The -it flag is erroring out as [explained here](https://stackoverflow.com/questions/43099116/error-the-input-device-is-not-a-tty/43099210#43099210)

*** makefile testenv target has the `docker-compose up -f` which is falling and it is working with  `docker-compose -f <compose.yml> up`.
As docker-compose v1 is deprecated, its suggested to use `docker compose -f`.

